package com.gem.report.persistence.repository;

import com.gem.report.persistence.entity.Part;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PartRepository extends PagingAndSortingRepository<Part, Long>, QueryDslPredicateExecutor {

}
