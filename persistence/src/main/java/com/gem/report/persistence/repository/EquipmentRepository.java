package com.gem.report.persistence.repository;

import com.gem.report.persistence.entity.Equipment;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EquipmentRepository extends PagingAndSortingRepository<Equipment, Long>, QueryDslPredicateExecutor {
}
