package com.gem.report.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "equipment")
public class Equipment {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @SequenceGenerator(name = "seq_equipment_id", sequenceName = "seq_equipment_id")
    @GeneratedValue(generator = "seq_equipment_id", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "color")
    private String color;

    @Column(name = "available")
    private Integer available;

    @Column(name = "postSold")
    private Integer postSold;

    @Column(name = "ordered")
    private Integer ordered;

    @Column(name = "ordered2")
    private Integer ordered2;

    @Column(name = "sold")
    private Integer sold;

    @Column(name = "sold2")
    private Integer sold2;

    @Column(name = "onOrder")
    private Integer onOrder;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getPostSold() {
        return postSold;
    }

    public void setPostSold(int postSold) {
        this.postSold = postSold;
    }

    public int getOrdered() {
        return ordered;
    }

    public void setOrdered(int ordered) {
        this.ordered = ordered;
    }

    public int getOrdered2() {
        return ordered2;
    }

    public void setOrdered2(int ordered2) {
        this.ordered2 = ordered2;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public int getSold2() {
        return sold2;
    }

    public void setSold2(int sold2) {
        this.sold2 = sold2;
    }

    public Integer getOnOrder() {
        return onOrder;
    }

    public void setOnOrder(Integer onOrder) {
        this.onOrder = onOrder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
