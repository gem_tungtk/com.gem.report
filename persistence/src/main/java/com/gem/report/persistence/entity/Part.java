package com.gem.report.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "part")
public class Part {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @SequenceGenerator(name = "seq_part_id", sequenceName = "seq_part_id")
    @GeneratedValue(generator = "seq_part_id", strategy = GenerationType.SEQUENCE)
    private String id;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "imp")
    private double imp;

    @Column(name = "imp_other")
    private double impOther;

    @Column(name = "exp")
    private double exp;

    @Column(name = "exp_other")
    private double expOther;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getImp() {
        return imp;
    }

    public void setImp(double imp) {
        this.imp = imp;
    }

    public double getImpOther() {
        return impOther;
    }

    public void setImpOther(double impOther) {
        this.impOther = impOther;
    }

    public double getExp() {
        return exp;
    }

    public void setExp(double exp) {
        this.exp = exp;
    }

    public double getExpOther() {
        return expOther;
    }

    public void setExpOther(double expOther) {
        this.expOther = expOther;
    }
}
