package com.gem.report.controller;

import com.gem.report.model.EquipmentModel;
import com.gem.report.persistence.entity.Equipment;
import com.gem.report.persistence.repository.EquipmentRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/equipment")
public class EquipmentController {

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    @Qualifier("jasper_equipments")
    private JasperReport equipmentReport;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<EquipmentModel> findAll() {
        List<EquipmentModel> equipments = new ArrayList<>();
        for (Equipment equipment : equipmentRepository.findAll()) {
            EquipmentModel equipmentModel = new EquipmentModel();
            BeanUtils.copyProperties(equipment, equipmentModel);
            equipments.add(equipmentModel);
        }
        return equipments;
    }

    @RequestMapping(value = "", params = "download", method = RequestMethod.GET, produces = {"application/pdf"})
    public ByteArrayResource exportAll(@RequestParam(name = "download") String download) throws JRException, IOException {
        List data = findAll();
        Map params = new HashMap();
        JasperPrint print = JasperFillManager.fillReport(equipmentReport, params, new JRBeanCollectionDataSource(data));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(print, outputStream);
        return new ByteArrayResource(outputStream.toByteArray());
    }

}
