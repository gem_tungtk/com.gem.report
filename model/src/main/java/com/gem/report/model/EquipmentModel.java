package com.gem.report.model;

public class EquipmentModel {

    private Long id;
    private String make;
    private String model;
    private String color;
    private Integer available;
    private Integer postSold;
    private Integer ordered;
    private Integer ordered2;
    private Integer sold;
    private Integer sold2;
    private Integer onOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getPostSold() {
        return postSold;
    }

    public void setPostSold(Integer postSold) {
        this.postSold = postSold;
    }

    public Integer getOrdered() {
        return ordered;
    }

    public void setOrdered(Integer ordered) {
        this.ordered = ordered;
    }

    public Integer getOrdered2() {
        return ordered2;
    }

    public void setOrdered2(Integer ordered2) {
        this.ordered2 = ordered2;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getSold2() {
        return sold2;
    }

    public void setSold2(Integer sold2) {
        this.sold2 = sold2;
    }

    public Integer getOnOrder() {
        return onOrder;
    }

    public void setOnOrder(Integer onOrder) {
        this.onOrder = onOrder;
    }
}
